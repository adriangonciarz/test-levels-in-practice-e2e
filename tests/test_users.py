from time import sleep

import allure
import requests

import endpoints


@allure.suite('Users Tests')
class TestUsers:
    """Tests for Users resource: """

    @allure.title('Create user and check details in /users list')
    def test_get_users_list(self, new_user_data):
        """Verify newly created user is present in the users list"""
        user_list_response = requests.get(endpoints.users)
        assert user_list_response.status_code == 200
        assert new_user_data in user_list_response.json()

    @allure.title('Create user and check details in /users/ID endpoint')
    def test_create_user_and_get_their_details(self, new_user_data):
        """Verify newly created user details are correct"""
        # Fetch user details, verify response
        created_user_id = new_user_data['id']

        user_details_response = requests.get(endpoints.user_with_id(created_user_id))

        assert user_details_response.status_code == 200
        assert user_details_response.json() == new_user_data
